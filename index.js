const process = require('process');
const moment = require('moment');
const mustache = require('mustache');
const fs = require('fs');

function createMigration() {
  const toTitleCase = (arr) => arr.map(s => s[0].toUpperCase() + s.slice(1));
  const inputArgs = process.argv.slice(2);
  const migrationName = toTitleCase(inputArgs).join('_');

  const timestamp = moment(new Date()).format('YYYYMMDDhhmmss');
  const fileName = `M_${timestamp}_${migrationName}`;

  const templateString = fs.readFileSync('migrationTemplate.cs').toString();
  const replacedTemplate = mustache.render(templateString, { migrationName, timestamp });

  const outFilePath = `C:\\Users\\Vagrant\\Source\\server\\${fileName}`;
  fs.writeFileSync(outFilePath, replacedTemplate);
}

if (process.argv.length > 2)
  createMigration();
else
  console.log('No migration name found. Ex: my great migration')