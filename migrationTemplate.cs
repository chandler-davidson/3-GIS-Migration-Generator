﻿using Migrator.Framework;

namespace ThreeGIS.Nx.Migrations.Migrations
{
    [Migration(MigrationTimestamp)]
    internal class M_{{timestamp}}_{{migrationName}} : Migration
    {
        private const long MigrationTimestamp = {{timestamp}};

        public override void Up()
        {

        }
    }
}