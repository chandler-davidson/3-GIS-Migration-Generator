const { compile } = require('nexe');

compile({
  input: 'index.js',
  output: 'MigrationGenerator.exe'
});